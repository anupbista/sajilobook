<?php
/**
 * The template for displaying the contact us page.
 *
 * @package sajilobooks
 */
?>

<?php get_header(); ?>

<header class="entry-header">
			<?php
			storefront_post_thumbnail( 'full' );
			the_title( '<h1 class="entry-title">', '</h1>' );
			?>
</header><!-- .entry-header -->

 <div class="contact">
    <div class="contact-map">
        <?php echo do_shortcode('[intergeo id="1gDN"][/intergeo]');?>    
      
    </div>
    <div class="contact-form">
        <?php echo do_shortcode('[contact-form-7 id="483" title="Sajilo Book Contact Form"]');?>    
    </div>
 </div>
 <?php get_footer(); ?>
