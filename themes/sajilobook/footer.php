<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

<script>
	var sajilobooksAlias = jQuery.noConflict();
	// Sticky Categories Sidbar
	sajilobooksAlias(".sticky").stick_in_parent();

// Need Help Tabs
sajilobooksAlias(document).ready(function(){
	sajilobooksAlias('ul.tabs li').click(function(){
		var tab_id = sajilobooksAlias(this).attr('data-tab');
		sajilobooksAlias('ul.tabs li').removeClass('current');
		sajilobooksAlias('.tab-content').removeClass('current');
		sajilobooksAlias(this).addClass('current');
		sajilobooksAlias("#"+tab_id).addClass('current');
	})
});

</script>
</body>
</html>
