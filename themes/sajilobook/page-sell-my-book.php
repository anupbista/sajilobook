<?php
/**
 * The template for displaying the Sell My Book.
 *
 * @package sajilobooks
 */
?>

<?php get_header(); ?>

<header class="entry-header">
			<?php
			storefront_post_thumbnail( 'full' );
			the_title( '<h1 class="entry-title">', '</h1>' );
			?>
</header><!-- .entry-header -->
<?php
if (is_user_logged_in()){
    
    do_action('sell_my_book_form');
}
else{
    ?>
        <div class="woocommerce-error">
            Please Login to Sell Your Books.
        </div>
 <?php  
} 
 get_footer(); 
