<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package SajiloBook
 */

get_header(); ?>

	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php

			/**
			 * Functions hooked in to homepage action
			 *
			 * @hooked storefront_homepage_content      - 10
			 * @hooked storefront_product_categories    - 20
			 * @hooked storefront_recent_products       - 30
			 * @hooked storefront_featured_products     - 40
			 * @hooked storefront_popular_products      - 50
			 * @hooked storefront_on_sale_products      - 60
			 * @hooked storefront_best_selling_products - 70
			 */
			do_action( 'homepage' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	
	<div id="secondary" class="widget-area sticky" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); 
	$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
	?>
	<div class="shop-link-btn">
	<a href="<?php echo $shop_page_url; ?>">Shop</a>
	</div>
	</div><!-- #secondary -->
<?php
get_footer();
