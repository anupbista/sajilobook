<?php
/**
 * The template for displaying the Need Help page.
 *
 * @package sajilobooks
 */
?>

<?php get_header(); ?>


	<ul class="tabs need-help-tabs">
		<li class="tab-link current" data-tab="tab-1">Discussion</li>
		<li class="tab-link" data-tab="tab-2">Getting Started</li>
	</ul>

	<div id="tab-1" class="tab-content current">
       <?php
// TO SHOW THE PAGE CONTENTS-->
while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
        <div class="entry-content-page">
            <?php comments_template(); ?> <!-- Page Comment -->
        </div><!-- .entry-content-page -->

    <?php
endwhile; //resetting the page loop
?>
	</div>
	<div id="tab-2" class="tab-content">
          <?php
// TO SHOW THE PAGE CONTENTS-->
while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
        <div class="entry-content-page">
            <?php the_content(); ?> <!-- Page Content -->
        </div><!-- .entry-content-page -->

    <?php
endwhile; //resetting the page loop
?>
	</div>

 <?php get_footer(); ?>
