<?php
/**
 * storefront child theme functions.php file.
 * @package sajilobook
 */

//Storefront adds it's own stylesheet for child themes

// Put your custom PHP below

function wpb_add_google_fonts() {
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"', false ); 
}

add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );


function sajilobook_css() {

    wp_enqueue_style( 'jquery.modal', get_stylesheet_directory_uri() . '/jquery.modal.min.css');
}
add_action( 'wp_enqueue_scripts', 'sajilobook_css' );


function sajilobooks_js() {
    wp_enqueue_script( 'sticky-kit', get_stylesheet_directory_uri() . '/sticky-kit.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'jquery-modal', get_stylesheet_directory_uri() . '/jquery.modal.min.js', array( 'jquery' ), '1.0', true );
}

add_action('wp_enqueue_scripts', 'sajilobooks_js');

// do_action('avia_meta_header') to get a logged in user name
add_action('avia_meta_header', 'ava_get_user');
function ava_get_user() {
	if ( is_user_logged_in()) {
		$user = wp_get_current_user();
		echo "<div id='header-welcome'>Welcome ". $user->display_name ."</div>";
	}
}


// Put Logged in user name in the secondary menu item

 if ( is_user_logged_in() ) { 
 	function give_profile_name(){
    $user=wp_get_current_user();
    $name=$user->user_firstname; 
    return $name;
} 
add_shortcode('profile_name', 'give_profile_name');

add_filter( 'wp_nav_menu_objects', 'my_dynamic_menu_items' );
function my_dynamic_menu_items( $menu_items ) {
    foreach ( $menu_items as $menu_item ) {
        if ( '#profile_name#' == $menu_item->title ) {
            
				$user = wp_get_current_user();
                $menu_item->title = "NAMASTE, ".$user->display_name;
        }
    }

    return $menu_items;
} 
}
 else { 

add_filter( 'wp_nav_menu_objects', 'my_dynamic_menu_items' );
function my_dynamic_menu_items( $menu_items ) {
    foreach ( $menu_items as $menu_item ) {
        if ( '#profile_name#' == $menu_item->title ) {
            $menu_item->title = "NAMASTE, SATHI";  
        }
    }

    return $menu_items;
} 
} 

// Put Account Login status name in the primary menu item

if ( is_user_logged_in() ) { 
add_filter( 'wp_nav_menu_objects', 'my_dynamic_menu_items_status' ); 	
function my_dynamic_menu_items_status( $menu_items ) {
    foreach ( $menu_items as $menu_item ) {
        if ( '#login_status#' == $menu_item->title ) {
        
                $menu_item->title = "My Account";
        }
    }

    return $menu_items;
} 
}
 else { 

add_filter( 'wp_nav_menu_objects', 'my_dynamic_menu_items_status' );
function my_dynamic_menu_items_status( $menu_items ) {
    foreach ( $menu_items as $menu_item ) {
        if ( '#login_status#' == $menu_item->title ) {
            $menu_item->title = "lOGIN / REGISTER";  
        }
    }

    return $menu_items;
} 
}

if ( is_user_logged_in() ) {
	add_filter( 'wp_nav_menu_objects', 'remove_wishlist_for_notlogged' ); 	
		function remove_wishlist_for_notlogged( $menu_items ) {
			foreach ( $menu_items as $menu_item ) {
				if ( '#wishlist#' == $menu_item->title ) {
					$menu_item->title = "MY WISHLIST";
				}
    	}
    	return $menu_items;
		}
}

if (!( is_user_logged_in() )) {
add_filter('wp_nav_menu_objects', 'ad_filter_menu', 10, 2);

function ad_filter_menu($sorted_menu_objects, $args) {

    // remove the menu item that has a title of 'Uncategorized'
    foreach ($sorted_menu_objects as $key => $menu_object) {

        // can also check for $menu_object->url for example
        // see all properties to test against:
        // print_r($menu_object); die();
        if ($menu_object->title == '#wishlist#') {
            unset($sorted_menu_objects[$key]);
            break;
        }
    }

    return $sorted_menu_objects;
}
}

// Add a Login / Logout nav menu
// add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );
// function add_loginout_link( $items, $args ) {
//     if (is_user_logged_in() && $args->theme_location == 'primary') {
//             $items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page fa fa-sign-out"><a class="menu-link" href="'. wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ) .'">Log Out</a></li>';
//     }
//     elseif (!is_user_logged_in() && $args->theme_location == 'primary') {
//             $items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page fa fa-key"><a class="menu-link" href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '">Log In / Register</a></li>';
//     }
//     return $items;
// }

// Disable searchwp notices
add_filter( 'searchwp_missing_integration_notices', '__return_false' );

// Woocommerce Custom Fields (eg: ISBN, Author etc)
// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

// Save Fields
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );

function woo_add_custom_general_fields() {

  global $woocommerce, $post;
  
  echo '<div class="options_group">';
    // ISBN Number
woocommerce_wp_text_input( 
	array( 
		'id'          => '_isbn', 
		'label'       => __( 'ISBN', 'woocommerce' ), 
		'placeholder' => 'ISBN Number',
		'desc_tip'    => 'true',
		'description' => __( 'Enter the ISBN number.', 'woocommerce' ) 
	)
);
// Author Name
woocommerce_wp_text_input( 
	array( 
		'id'          => '_author', 
		'label'       => __( 'Author', 'woocommerce' ), 
		'placeholder' => 'Author Name',
		'desc_tip'    => 'true',
		'description' => __( 'Enter the name of the Author.', 'woocommerce' ) 
	)
);
// Publisher
woocommerce_wp_text_input( 
	array( 
		'id'          => '_publisher', 
		'label'       => __( 'Publisher', 'woocommerce' ), 
		'placeholder' => 'Publisher Name',
		'desc_tip'    => 'true',
		'description' => __( 'Enter the name of the Publisher.', 'woocommerce' ) 
	)
);
// Language
woocommerce_wp_text_input( 
	array( 
		'id'          => '_language', 
		'label'       => __( 'Language', 'woocommerce' ), 
		'placeholder' => 'Language',
		'desc_tip'    => 'true',
		'description' => __( 'Language', 'woocommerce' ) 
	)
);
// Pages
woocommerce_wp_text_input( 
	array( 
		'id'          => '_pages', 
		'label'       => __( 'Pages', 'woocommerce' ), 
		'placeholder' => 'Pages',
		'desc_tip'    => 'true',
		'description' => __( 'Number of pages', 'woocommerce' ) 
	)
);

// Book Type
woocommerce_wp_select( 
array( 
	'id'      => '_book_type', 
	'label'   => __( 'Book Type', 'woocommerce' ), 
	'options' => array(
		'FIRST Hand'   => __( 'FIRST Hand', 'woocommerce' ),
		'SECOND Hand'   => __( 'SECOND Hand', 'woocommerce' ),
		),
	'desc_tip'    => 'true',
	'description' => __( 'Select the type of Book', 'woocommerce' ) 
)
);
// Book Condition
woocommerce_wp_text_input( 
	array( 
		'id'          => '_book_condition', 
		'label'       => __( 'Book Condition', 'woocommerce' ), 
		'placeholder' => 'Book Condition',
		'desc_tip'    => 'true',
		'description' => __( 'Enter the condition of the Book', 'woocommerce' ) 
	)
);
// Seller
woocommerce_wp_text_input( 
	array( 
		'id'          => '_seller', 
		'label'       => __( 'Seller', 'woocommerce' ), 
		'placeholder' => 'Seller',
		'desc_tip'    => 'true',
		'description' => __( 'Enter the Seller of the Book', 'woocommerce' ) 
	)
);
  echo '</div>';
	
}
function woo_add_custom_general_fields_save( $post_id ){
	
	// ISBN
	$woocommerce_text_field = $_POST['_isbn'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_isbn', esc_attr( $woocommerce_text_field ) );

    // Author
	$woocommerce_text_field = $_POST['_author'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_author', esc_attr( $woocommerce_text_field ) );

    // Publisher
	$woocommerce_text_field = $_POST['_publisher'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_publisher', esc_attr( $woocommerce_text_field ) );

    // Language
	$woocommerce_text_field = $_POST['_language'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_language', esc_attr( $woocommerce_text_field ) );
    
    // Pages
	$woocommerce_text_field = $_POST['_pages'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_pages', esc_attr( $woocommerce_text_field ) );

	// Book Type
	$woocommerce_book_type = $_POST['_book_type'];
	if( !empty( $woocommerce_book_type ) )
		update_post_meta( $post_id, '_book_type', esc_attr( $woocommerce_book_type ) );
		
	// Book Condition
	$woocommerce_book_condition = $_POST['_book_condition'];
	if( !empty( $woocommerce_book_condition ) )
		update_post_meta( $post_id, '_book_condition', esc_attr( $woocommerce_book_condition ) );

	// Seller
	$woocommerce_seller = $_POST['_seller'];
	if( !empty( $woocommerce_seller ) )
		update_post_meta( $post_id, '_seller', esc_attr( $woocommerce_seller ) );		
    
}


// Remove Woocommerce Hooks
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

// SajiloBooks Books Description Format
add_action( 'woocommerce_single_product_summary', 'sajilobooks_description', 60 );

if ( ! function_exists( 'sajilobooks_description' ) ) {

	/**
	 * Output the Description(content)
	 *
	 */
	function sajilobooks_description() {
		wc_get_template( 'woocommerce/single-product/tabs/description.php' );
	}
}

// SajiloBook Book Description Hooks
add_action( 'sajilobook_book_description', 'sajilobook_category',5 );
add_action( 'sajilobook_book_description', 'sajilobook_isbn',10 );
add_action( 'sajilobook_book_description', 'sajilobook_author',15 );
add_action( 'sajilobook_book_description', 'sajilobook_publisher',20 );
add_action( 'sajilobook_book_description', 'sajilobook_language',25 );
add_action( 'sajilobook_book_description', 'sajilobook_pages',30 );
add_action( 'sajilobook_book_description', 'sajilobook_tags',30 );


if ( ! function_exists( 'sajilobook_isbn' ) ) {

	/**
	 * Output the ISBN Number
	 *
	 */
	function sajilobook_isbn() {
		$isbn_number = get_post_meta( get_the_ID(), '_isbn', true );
		if (!empty( $isbn_number )){
			echo '<li><label>ISBN: </label>'.$isbn_number.'</li>';
		}
		else{
			echo '<li><label>ISBN: </label>N/A</li>';
			
		}
	}
}

if ( ! function_exists( 'sajilobook_author' ) ) {

	/**
	 * Output the Author Name
	 *
	 */
	function sajilobook_author() {
		$sajilobook_author = get_post_meta( get_the_ID(), '_author', true );
		if (!empty( $sajilobook_author )){
			echo '<li><label>Author: </label>'.$sajilobook_author.'</li>';
		}
		else{
			echo '<li><label>Author: </label>N/A</li>';
			
		}
	}
}

if ( ! function_exists( 'sajilobook_publisher' ) ) {

	/**
	 * Output the Publisher
	 *
	 */
	function sajilobook_publisher() {
		$sajilobook_publisher = get_post_meta( get_the_ID(), '_publisher', true );
		if (!empty( $sajilobook_publisher )){
			echo '<li><label>Publisher: </label>'.$sajilobook_publisher.'</li>';
		}
		else{
			echo '<li><label>Publisher: </label>N/A</li>';
			
		}
	}
}

if ( ! function_exists( 'sajilobook_language' ) ) {

	/**
	 * Output the Langauge
	 *
	 */
	function sajilobook_language() {
		$sajilobook_language = get_post_meta( get_the_ID(), '_language', true );
		if (!empty( $sajilobook_language )){
			echo '<li><label>Language: </label>'.$sajilobook_language.'</li>';
		}
		else{
			echo '<li><label>Language: </label>N/A</li>';
			
		}
	}
}

if ( ! function_exists( 'sajilobook_pages' ) ) {

	/**
	 * Output the Pages
	 *
	 */
	function sajilobook_pages() {
		$sajilobook_pages = get_post_meta( get_the_ID(), '_pages', true );
		if (!empty( $sajilobook_pages )){
			echo '<li><label>Pages: </label>'.$sajilobook_pages.'</li>';
		}
		else{
			echo '<li><label>Pages: </label>N/A</li>';
			
		}
	}
}

if ( ! function_exists( 'sajilobook_category' ) ) {

	/**
	 * Output the Category
	 *
	 */
	function sajilobook_category() {
	global $product;
	$sajilobook_category = wc_get_product_category_list( $product->get_id(), ', ', '<label class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</label>' );	
	if (!empty( $sajilobook_category )){	
		echo '<li>'.$sajilobook_category.'</li>';		
	}
	else{
		echo '<li><label>Category: </label>N/A</li>';		
	}
	}
}
if ( ! function_exists( 'sajilobook_tags' ) ) {

	/**
	 * Output the Category
	 *
	 */
	function sajilobook_tags() {
	global $product;
	$sajilobook_tags = wc_get_product_tag_list( $product->get_id(), ', ', '<label class="posted_in">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</label>' );	
	if (!empty( $sajilobook_tags )){	
		echo '<li>'.$sajilobook_tags.'</li>';		
	}
	else{
		echo '<li><label>Category: </label>N/A</li>';		
	}
	}
}
if ( ! function_exists( 'sajilobook_book_type' ) ) {

	/**
	 * Output the Book Type
	 *
	 */
	function sajilobook_book_type() {
		$sajilobook_book_type = get_post_meta( get_the_ID(), '_book_type', true );
		if (!empty( $sajilobook_book_type )){
			echo $sajilobook_book_type;
		}
		else{
			echo 'N/A';
			
		}
	}
}
if ( ! function_exists( 'sajilobook_book_condition' ) ) {

	/**
	 * Output the Book Condition
	 *
	 */
	function sajilobook_book_condition() {
		$sajilobook_book_condition = get_post_meta( get_the_ID(), '_book_condition', true );
		if (!empty( $sajilobook_book_condition )){
			echo $sajilobook_book_condition;
		}
		else{
			echo 'N/A';
			
		}
	}
}
if ( ! function_exists( 'sajilobook_book_seller' ) ) {

	/**
	 * Output the Seller
	 *
	 */
	function sajilobook_book_seller() {
		$sajilobook_book_seller = get_post_meta( get_the_ID(), '_seller', true );
		if (!empty( $sajilobook_book_seller )){
			echo $sajilobook_book_seller;
		}
		else{
			echo 'N/A';
			
		}
	}
}

if ( ! function_exists( 'sajilobook_book_price' ) ) {

	/**
	 * Output the Book price
	 *
	 */
	function sajilobook_book_price() {
		global $product;
		$sajilobook_book_price =  $product->get_price_html();
		if (!empty( $sajilobook_book_price )){
			echo $sajilobook_book_price;
		}
		else{
			echo 'N/A';
			
		}
	}
}


if ( ! function_exists( 'woocommerce_default_product_tabs' ) ) {

	/**
	 * Add default product tabs to product pages.
	 *
	 * @param array $tabs
	 * @return array
	 */
	function woocommerce_default_product_tabs( $tabs = array() ) {
		global $product, $post;

		// Description tab - shows product content
		// if ( $post->post_content ) {
		// 	$tabs['description'] = array(
		// 		'title'    => __( 'Description', 'woocommerce' ),
		// 		'priority' => 10,
		// 		'callback' => 'woocommerce_product_description_tab',
		// 	);
		// }

		// Additional information tab - shows attributes
		if ( $product && ( $product->has_attributes() || apply_filters( 'wc_product_enable_dimensions_display', $product->has_weight() || $product->has_dimensions() ) ) ) {
			$tabs['additional_information'] = array(
				'title'    => __( 'Additional information', 'woocommerce' ),
				'priority' => 20,
				'callback' => 'woocommerce_product_additional_information_tab',
			);
		}

		// Reviews tab - shows comments
		if ( comments_open() ) {
			$tabs['reviews'] = array(
				'title'    => sprintf( __( 'Reviews (%d)', 'woocommerce' ), $product->get_review_count() ),
				'priority' => 30,
				'callback' => 'comments_template',
			);
		}

		return $tabs;
	}
}

if( !function_exists ( sajilobook_publisher_item_loop )){
	function sajilobook_publisher_item_loop(){
		$sajilobook_publisher_item = get_post_meta( get_the_ID(), '_publisher', true );
		if (!empty( $sajilobook_publisher_item )){
			echo '<ins class="sajilobook_loop_publisher">'.$sajilobook_publisher_item.'<ins>';
		}
		else{
			echo '<ins class="sajilobook_loop_publisher">Publication: N/A</ins>';			
		}
}
}


remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'sajilobook_publisher_item_loop',5 );

/**
 * Change the add to cart text on single product pages
 */
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );

function woo_custom_cart_button_text() {

	global $woocommerce;
	
	foreach($woocommerce->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];
	
		if( get_the_ID() == $_product->id ) {
			return __('Already in cart - Add Again?', 'woocommerce');
		}
	}
	
	return __('Add to cart', 'woocommerce');
}

/**
 * Change the add to cart text on product archives
 */
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );

function woo_archive_custom_cart_button_text() {

	global $woocommerce;
	
	foreach($woocommerce->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];
	
		if( get_the_ID() == $_product->id ) {
			return __('Add Again?', 'woocommerce');
		}
	}
	
	return __('Add to cart', 'woocommerce');
}

// Replace shop page title

add_filter( 'woocommerce_page_title', 'woo_shop_page_title');

function woo_shop_page_title( $page_title ) {
	
	if( 'Shop' == $page_title) {
		return "Book Store";
	}
}


/**
 * Check Product Type (Simple, Variable, Grouped, Bundle etc)
 * is_type can be simple, variable, grouped, external
 * woocommerce_single_product_summary is hook!
 * Put this into your functions.php (child-theme)
**/
// add_action( 'woocommerce_single_product_summary', 'get_product_type',  5 );
// function get_product_type() {
//         global $post;
//             if( function_exists('get_product') ){
//                 $product = get_product( $post->ID );
//                 if( $product->is_type( 'yith_bundle' ) ){
                
//         }
//     }
// }


// No of Books in one row in Shop page(archieve.php)
add_filter( 'storefront_loop_columns', 'sf_child_products_per_row' );
function sf_child_products_per_row() {
	return 4;
}


// Add a wishlist button in a product loop (archieve pages)
if( !function_exists ( sajilobook_add_to_wishlist )){
	function sajilobook_add_to_wishlist(){
		echo do_shortcode('[yith_wcwl_add_to_wishlist]');		
}
}
// add_action( 'woocommerce_after_shop_loop_item_title','sajilobook_add_to_wishlist',10 );


// Add a Bundle mark on a Bundle Product
if( !function_exists ( sajilobook_add_bundle_mark )){
	function sajilobook_add_bundle_mark(){
		global $post;
            if( function_exists('get_product') ){
                $product = get_product( $post->ID );
                if( $product->is_type( 'yith_bundle' ) ){
					echo '<span class="bundled-mark">BUNDLE</span>';						
        }
	}
}}
add_action( 'woocommerce_before_shop_loop_item_title','sajilobook_add_bundle_mark',10 );

// Add a Bundle mark on a Bundle OR Single Product
if( !function_exists ( sajilobook_add_grouped_mark )){
	function sajilobook_add_grouped_mark(){
		global $post;
            if( function_exists('get_product') ){
                $product = get_product( $post->ID );
                if( $product->is_type( 'grouped' ) ){
					echo '<span class="grouped-mark">BUNDLE / SINGLE</span>';						
        }
	}
}}
add_action( 'woocommerce_before_shop_loop_item_title','sajilobook_add_grouped_mark',10 );

// Redefine woocommerce_output_related_products()
function woocommerce_output_related_products() {
woocommerce_related_products(4,1); // Display 4 products in rows of 1
}

// Redirect to Homepage after login
add_filter('woocommerce_login_redirect', 'sb_login_redirect');
function sb_login_redirect( $redirect_to ) {
     $redirect_to = home_url();
     return $redirect_to;
}

// Redirect to Homepage after registration
add_filter('woocommerce_registration_redirect', 'sb_register_redirect');
function sb_register_redirect( $redirect_to ) {
     $redirect_to = home_url();
     return $redirect_to;
}





// Register Custom Post Type
function books_received() {
  $labels = array(
    'name' => _x('Books Received', 'the custom posts', 'your_text_domain'),
    'singular_name' => _x('Books Received', 'the custom post', 'your_text_domain'),
    'all_items' => __('All Books Received', 'your_text_domain'),
    'view_item' => __('View Books Received', 'your_text_domain'),
    'edit_item' => __('View Books Received', 'your_text_domain'),
    'search_items' => __('Search Books Received', 'your_text_domain'),
    'not_found' =>  __('No Books Received found', 'your_text_domain'),
    'not_found_in_trash' => __('No Books Received found in Trash', 'your_text_domain'), 
    'parent_item_colon' => '',
    'menu_name' => __('Books Received', 'your_text_domain')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => false,
    'show_ui' => true, 
    'show_in_menu' => true, 
	'capabilities' 		  => array('create_posts' => false),
	'map_meta_cap' => true,
	'menu_position'       => 58,
	'menu_icon'           => 'dashicons-book-alt',
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'books-received', 'URL slug', 'your_text_domain' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title','thumbnail' )
  ); 
  register_post_type('books_received', $args);
}
add_action( 'init', 'books_received' );

// Hide quick edit etc..
add_filter( 'post_row_actions', 'remove_row_actions', 10, 1 );
function remove_row_actions( $actions )
{
    if( get_post_type() === 'books_received' )
        unset( $actions['edit'] );
        unset( $actions['view'] );
        unset( $actions['inline hide-if-no-js'] );
    return $actions;
}


/* Extra Fields in WooCommerce Registration */

/**
 * Add new register fields for WooCommerce registration.
 *
 * @return string Register fields HTML.
 */
function sajilobook_extra_register_fields() {
    ?>

    <p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e( 'First Name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>

    <p class="form-row form-row-last">
    <label for="reg_billing_last_name"><?php _e( 'Last Name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>

    <p class="form-row form-row-wide">
    <label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
    </p>

	<!--<p class="form-row form-row-wide">
    <label for="reg_billing_city"><?php _e( 'City', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_city" id="reg_billing_city" value="<?php if ( ! empty( $_POST['billing_city'] ) ) esc_attr_e( $_POST['billing_city'] ); ?>" />
    </p>

    <p class="form-row form-row-wide">
    <label for="reg_billing_address_1"><?php _e( 'Address', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_address_1" id="reg_billing_address_1" value="<?php if ( ! empty( $_POST['billing_address_1'] ) ) esc_attr_e( $_POST['billing_address_1'] ); ?>" />
    </p>-->

    <?php
}

add_action( 'woocommerce_register_form_start', 'sajilobook_extra_register_fields' );

/**
 * Validate the extra register fields.
 *
 * @param  string $username          Current username.
 * @param  string $email             Current email.
 * @param  object $validation_errors WP_Error object.
 *
 * @return void
 */
function sajilobook_validate_extra_register_fields( $username, $email, $validation_errors ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $validation_errors->add( 'billing_first_name_error', __( 'Enter the First Name', 'woocommerce' ) );
    }

    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $validation_errors->add( 'billing_last_name_error', __( 'Enter the Last Name', 'woocommerce' ) );
    }


    if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
        $validation_errors->add( 'billing_phone_error', __( 'Enter the Phone', 'woocommerce' ) );
    }

    // if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
    //     $validation_errors->add( 'billing_address_1_error', __( 'Enter the Address', 'woocommerce' ) );
    // }

    // if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
    //     $validation_errors->add( 'billing_city_error', __( 'Enter the City', 'woocommerce' ) );
    // }

}

add_action( 'woocommerce_register_post', 'sajilobook_validate_extra_register_fields', 10, 3 );

/**
 * Save the extra register fields.
 *
 * @param  int  $customer_id Current customer ID.
 *
 * @return void
 */
function sajilobook_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        // WordPress default first name field.
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );

        // WooCommerce billing first name.
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    }

    if ( isset( $_POST['billing_last_name'] ) ) {
        // WordPress default last name field.
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );

        // WooCommerce billing last name.
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    }

    if ( isset( $_POST['billing_phone'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
    }

    // if ( isset( $_POST['billing_address_1'] ) ) {
    //     // WooCommerce billing address
    //     update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
    // }

    // if ( isset( $_POST['billing_city'] ) ) {
    //     // WooCommerce billing city
    //     update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
    // }

}

add_action( 'woocommerce_created_customer', 'sajilobook_save_extra_register_fields' );


// Remove Fields from Woocommerce
add_filter( 'woocommerce_checkout_fields' , 'sajilobook_remove_billing_checkout_field' );
function sajilobook_remove_billing_checkout_field( $fields ) {
  unset($fields['billing']['billing_postcode']);
  unset($fields['billing']['billing_state']);
  return $fields;
}

add_filter( 'woocommerce_billing_fields' , 'sajilobook_remove_billing_address_field' );
function sajilobook_remove_billing_address_field( $fields ) {
  unset($fields['billing_postcode']);
  unset($fields['billing_state']);
  return $fields;
}

add_filter( 'woocommerce_shipping_fields' , 'sajilobook_remove_shipping_address_field' );
function sajilobook_remove_shipping_address_field( $fields ) {
  unset($fields['shipping_postcode']);
  unset($fields['shipping_state']);
  return $fields;
}

// Change Placeholder, label and other options for Checkout page

add_filter( 'woocommerce_checkout_fields' , 'sajilobook_override_billing_checkout_field' );
function sajilobook_override_billing_checkout_field( $fields ) {
     $fields['billing']['billing_address_1']['label'] = 'Address';
     $fields['billing']['billing_address_1']['placeholder'] = 'Address';
     $fields['billing']['billing_address_2']['placeholder'] = 'Landmark (Building, etc)';
     $fields['billing']['billing_address_2']['required'] = false;
	 return $fields;	 
}

add_filter( 'woocommerce_billing_fields' , 'sajilobook_override_billing_address_field' );
function sajilobook_override_billing_address_field( $fields ) {
     $fields['billing_address_1']['label'] = 'Address';
     $fields['billing_address_1']['placeholder'] = 'Address';
     $fields['billing_address_2']['placeholder'] = 'Landmark (Building, etc)';
     $fields['billing_address_2']['required'] = false;	 
	 return $fields;	 
}

add_filter( 'woocommerce_shipping_fields' , 'sajilobook_override_shipping_address_field' );
function sajilobook_override_shipping_address_field( $fields ) {
     $fields['shipping_address_1']['label'] = 'Address';
     $fields['shipping_address_1']['placeholder'] = 'Address';
     $fields['shipping_address_2']['placeholder'] = 'Landmark (Building, etc)';
     $fields['shipping_address_2']['required'] = false;	 
	 return $fields;	 
}


// BreadCrumb 

add_filter( 'woocommerce_breadcrumb_defaults', 'sajilobook_change_breadcrumb_home_text' );
function sajilobook_change_breadcrumb_home_text( $defaults ) {
	$defaults['home'] = 'Home';
	return $defaults;
}

//My Account Details name change
function sajilobooks_my_account_change() {
 $myorder = array(
 'dashboard' => __( 'Dashboard', 'woocommerce' ),
 'orders' => __( 'Orders', 'woocommerce' ),
 'downloads' => __( 'Downloads', 'woocommerce' ),
 'edit-account' => __( 'Details', 'woocommerce' ),
 'edit-address' => __( 'Addresses', 'woocommerce' ),
 'customer-logout' => __( 'Logout', 'woocommerce' ),
 );
 return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'sajilobooks_my_account_change' ); 


// Fields for Sell My Book Page

function sajilobook_sell_my_book_fields() {
    ?>

	<div class="sell_my_book_form card">
	<form method="post" action="" enctype="multipart/form-data">
		
    <p class="form-row form-row-wide">
    <label for="selling_book_name"><?php _e( 'Book Name', 'sajilobbook' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="selling_book_name" id="selling_book_name" required="required"/>
    </p>

    <p class="form-row form-row-wide">
    <label for="selling_publication_name"><?php _e( 'Publication', 'sajilobbook' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="selling_publication_name" id="selling_publication_name" required="required"/>
    </p>

    <p class="form-row form-row-wide">
    <label for="selling_author_name"><?php _e( 'Author Name', 'sajilobbook' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="selling_author_name" id="selling_author_name" required="required"/>
    </p>

	<p class="form-row form-row-wide">
    <label for="selling_book_category"><?php _e( 'Category', 'sajilobbook' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="selling_book_category" id="selling_book_category" required="required"/>
    </p>

	<p class="form-row form-row-wide">
    <label for="selling_book_type"><?php _e( 'Book Type', 'sajilobbook' ); ?> <span class="required">*</span></label>
    <select name="selling_book_type" id="selling_book_type" class="input-text" required="required">
		       <?php 
                    $option_values = array('First Hand', 'Second Hand');

                    foreach($option_values as $key => $value) 
                    {
                            ?>
                                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                            <?php
                    }
                ?>
	</select>
    </p>

    <p class="form-row form-row-wide">
    <label for="selling_book_isbn"><?php _e( 'ISBN (without -)', 'sajilobbook' ); ?></label>
    <input type="text" class="input-text" name="selling_book_isbn" id="selling_book_isbn" />
    </p>

	<p class="form-row form-row-wide">
    <label for="selling_book_address"><?php _e( 'Your Address', 'sajilobbook' ); ?></label>
    <input type="text" class="input-text" name="selling_book_address" id="selling_book_address" />
    </p>

	<!--Book Image -->
	<p class="form-row form-row-wide">
    <label for="selling_book_image"><?php _e( 'Book Image', 'sajilobbook' ); ?> <span class="required">*</span></label>
    <input type="file" class="input-text" name="selling_book_image" id="selling_book_image" accept="image/*"  required="required" >
    </p>
	
	<p class="form-row clearfix">
		<input type="submit" class="woocommerce-Button button" name="sell_my_book_submit" value="<?php esc_attr_e( 'Submit', 'sajilobbook' ); ?>" />
	</p>
	
	<input type="hidden" name="post-type" id="post-type" value="books_received" />
	<input type="hidden" name="action" value="books_received" />
		<?php wp_nonce_field( 'sajilobook_sell_my_book', 'sajilobook_sell_my_book-nonce' ); ?>
	</form>
</div>
<?php
	if($_POST){
		sajilobook_save_sell_book_data();
	}
	
}
add_action('sell_my_book_form','sajilobook_sell_my_book_fields');


function sajilobook_save_sell_book_data() {

	if ( empty($_POST) || !wp_verify_nonce($_POST['sajilobook_sell_my_book-nonce'],'sajilobook_sell_my_book') )
	{
	   wc_print_notice( __( 'Sorry, your nonce did not verify.', 'sajilobook' ), 'error' );
	//    exit;
	}else{ 
 
		// Do some minor form validation to make sure there is content

		if (isset ($_POST['selling_book_name'])) {
			$selling_book_name =  $_POST['selling_book_name'];
		} else {
			wc_print_notice( __( 'Enter a Book Name.', 'sajilobook' ), 'error' );
			// exit;
		}
		if (isset ($_POST['selling_publication_name'])) {
			$selling_publication_name =  $_POST['selling_publication_name'];
		} else {
			wc_print_notice( __( 'Enter a Publcation Name.', 'sajilobook' ), 'error' );
			// exit;
		}
		if (isset ($_POST['selling_author_name'])) {
			$selling_author_name =  $_POST['selling_author_name'];
		} else {
			wc_print_notice( __( 'Enter Author Name.', 'sajilobook' ), 'error' );
			// exit;
		}
		if (isset ($_POST['selling_book_address'])) {
			$selling_book_address =  $_POST['selling_book_address'];
		} else {
			wc_print_notice( __( 'Enter Your Address.', 'sajilobook' ), 'error' );
			// exit;
		}
		if (isset ($_POST['selling_book_category'])) {
			$selling_book_category =  $_POST['selling_book_category'];
		} else {
			wc_print_notice( __( 'Enter the Book Category.', 'sajilobook' ), 'error' );
			// exit;
		}
		$selling_book_isbn = $_POST['selling_book_isbn'];

		// Add the content of the form to $post as an array
		$books_received = array(
			'post_title' => wp_strip_all_tags( $selling_book_name ),
			'post_status' => 'not_bought',			// Choose: publish, preview, future, etc.
			'post_type' => 'books_received',  // Use a custom post type if you want to
		);
		$books_received_post = wp_insert_post($books_received);  // http://codex.wordpress.org/Function_Reference/wp_insert_postsss


		    if (!function_exists('wp_generate_attachment_metadata')){
                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                require_once(ABSPATH . "wp-admin" . '/includes/file.php');
                require_once(ABSPATH . "wp-admin" . '/includes/media.php');
            }
             if ($_FILES) {
                foreach ($_FILES as $file => $array) {
                    if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) {
                        return "Upload error : " . $_FILES[$file]['error'];
                    }
                    $attach_id = media_handle_upload( $file, $books_received_post );
                }   
            }
            if ($attach_id > 0){
                //and if you want to set that image as Post  then use:
                update_post_meta($books_received_post,'_thumbnail_id',$attach_id);
            }

			// wc_print_notice( __( 'Thank You for submitting. We will contact you shortly.', 'sajilobook' ), 'success' );

			echo '<div class="woocommerce-message sajilobook-success">Thank You for submitting. We will contact you shortly.</div>';

		// $location = home_url(); // redirect location 

        // echo "<meta http-equiv='refresh' content='0;url=$location' />"; exit;
	} // end IF

}

// Custom Post Type Status
function sajilobook_custom_post_status(){
     register_post_status( 'not_bought', array(
          'label'                     => _x( 'Not Bought', 'books_received' ),
          'public'                    => false,
          'show_in_admin_all_list'    => false,
          'show_in_admin_status_list' => true,
          'label_count'               => _n_noop( 'Not Bought <span class="count">(%s)</span>', 'Not Bought <span class="count">(%s)</span>' )
     ) );
}
add_action( 'init', 'sajilobook_custom_post_status' );


// Change Default Meta Box 
add_action( 'admin_head', 'books_received_book_image' );
function books_received_book_image() {
    add_meta_box('postimagediv', __('Book Image'), 'post_thumbnail_meta_box', 'books_received', 'side', 'high');
	add_meta_box( 'submitdiv', __( 'Book Status' ), 'post_submit_meta_box', 'books_received', 'side', 'high' ); 
}


function book_received_add_book_details()
{
    add_meta_box("book_received_book_details", "Details of the Book Received", "books_received_add_details_meta_box", "books_received", "normal", "high", null);
}

add_action("add_meta_boxes", "book_received_add_book_details");

function books_received_add_details_meta_box($post)
{
$selling_author_name = get_post_meta($post->ID, 'selling_author_name', true);
$selling_publication_name = get_post_meta($post->ID, 'selling_publication_name', true);
$selling_book_category = get_post_meta($post->ID, 'selling_book_category', true);
$selling_book_type = get_post_meta($post->ID, 'selling_book_type', true);
$selling_book_isbn = get_post_meta($post->ID, 'selling_book_isbn', true);
$selling_book_address = get_post_meta($post->ID, 'selling_book_address', true);
$author = get_the_author();
$author_id = $post->post_author;
?>

<div class="books_received_inf clearfix">
	<div class="book_detail">
		<p class="detail-title">Book Details</p>
		<p>
			<label for="selling_author_name">Book Name</label>
			<input type="text" readonly value="<?php the_title();?>">
		</p>
		<p>
			<label for="selling_publication_name">Author Name</label>
			<input type="text" readonly name="selling_author_name" value="<?php echo $selling_author_name;?>">
		</p>
		<p>
			<label for="selling_publication_name">Publication Name</label>
			<input type="text" readonly name="selling_publication_name" value="<?php echo $selling_publication_name;?>">
		</p>
		<p>
			<label for="selling_book_category">Book Category</label>
			<input type="text" readonly name="selling_book_category" value="<?php echo $selling_book_category;?>">
		</p>
		<p>
			<label for="selling_book_type">Book Type</label>
			<input type="text" readonly name="selling_book_type" value="<?php echo $selling_book_type;?>">
		</p>
		<p>
			<label for="selling_book_isbn">Book ISBN</label>
			<input type="text" readonly name="selling_book_isbn" value="<?php echo $selling_book_isbn;?>">
		</p>
	</div>
	<div class="customer_detail">
		<p class="detail-title">Customer Details</p>
		<p>
			<label for="customer_user_name">Customer Username</label>
			<input type="text" readonly value="<?php echo the_author_meta( 'user_nicename' , $author_id );?>">
		</p>
		<p>
			<label for="customer_first_name">First Name</label>
			<input type="text" readonly value="<?php echo the_author_meta( 'first_name' , $author_id );?>">			
		</p>
		<p>
			<label for="customer_phone">Last Name</label>
			<input type="text" readonly value="<?php echo the_author_meta( 'last_name' , $author_id );?>">			
		</p>
		<p>
			<label for="customer_address">Phone</label>
			<input type="text" readonly value="<?php echo the_author_meta( 'billing_phone' , $author_id );?>">			
		</p>
		<p>
			<label for="customer_email">Email</label>
			<input type="text" readonly value="<?php echo the_author_meta( 'email' , $author_id );?>">			
		</p>
		<p>
			<label for="selling_book_address">Address</label>
			<input type="text" readonly value="<?php echo $selling_book_address;?>">			
		</p>
	</div>
</div>

<?php    

}


add_action('admin_head', 'admin_book_received_styles');

function admin_book_received_styles() {
  echo '<style>
   	.book_detail{
		float:left;
		width:50%;
	   }
	   .customer_detail{
		width:50%;
		float:left;
	   }
	   .clearfix:after{
		   clear:both;
		   display: block;
		   content: "";
	   }
	   .book_detail input,
	   .book_detail label,
	   .customer_detail input,
	   .customer_detail label{
			width: 50%;
			display:inline-block;
	   }	
	   .book_detail input,
	   .customer_detail input{
		//    background:transparent;
		   box-shadow: none;
		   border: none;
	   }
	   .detail-title,
	   .book_detail label,
	   .customer_detail label{
		   font-size: 16px;
		   margin-bottom: 2px;
	   }
	   .detail-title{
		   border-bottom: 1px solid #e2e2e2;
		   padding-bottom:10px;
	   }
  </style>';
}


function sajilobooks_save_postdata($post_id)
{
    if (array_key_exists('selling_author_name', $_POST)) {
        update_post_meta(
            $post_id,
            'selling_author_name',
            $_POST['selling_author_name']
        );
    }
	if (array_key_exists('selling_publication_name', $_POST)) {
        update_post_meta(
            $post_id,
            'selling_publication_name',
            $_POST['selling_publication_name']
        );
    }
	if (array_key_exists('selling_book_category', $_POST)) {
        update_post_meta(
            $post_id,
            'selling_book_category',
            $_POST['selling_book_category']
        );
    }
	if (array_key_exists('selling_book_type', $_POST)) {
        update_post_meta(
            $post_id,
            'selling_book_type',
            $_POST['selling_book_type']
        );
    }
	if (array_key_exists('selling_book_isbn', $_POST)) {
        update_post_meta(
            $post_id,
            'selling_book_isbn',
            $_POST['selling_book_isbn']
        );
    }
	if (array_key_exists('selling_book_address', $_POST)) {
        update_post_meta(
            $post_id,
            'selling_book_address',
            $_POST['selling_book_address']
        );
    }
}
add_action('save_post', 'sajilobooks_save_postdata');
